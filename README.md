# maven-dependency-proxy

## Context

This project is the home for the Maven dependency proxy demo. It's intended to quickly summarize what the feature is, why it's important for software developers and platform engineers, and, of course, a demo. 

## What is the Maven dependency proxy?

A typical software project relies on a variety of dependencies, which we call Packages. Packages can be internally built and maintained, or sourced from a public repository. Based on our user research, we’ve learned that most projects use a 50/50 mix of public vs. private packages. When installing packages, the order in which they are found and downloaded is very important, as downloading/using an incorrect package or version of a package can introduce breaking changes and security vulnerabilities into their pipelines.

The new feature expands the functionanlity of the package registry to give users the ability to add/configure one external Java repository. Once added, when a user tries to install a Java package using their project-level endpoint, GitLab will first look for the package in the project and if it's not found, will attempt to pull the package from the external repository.

When a package is pulled from the external repository it will be imported into the GitLab project so that the next time that particular package/version is pulled it's pulled from GitLab and not the external repository.
        
If the package is not found in their GitLab project or the external repository we will return an error.
      
- This feature and all future dependency proxy formats will be in the Premium tier.
- Project owners will be able to configure this via a project's settings (API or UI)
- We will support external repositories that require authentication, such as Artifactory or Sonatype

## Why is this important?
        
This functionality will: 

1. Make pipelines faster.
1. Make pipelines more reliable.
1. It will reduce the cost of data transfer since over time most packages will be pulled from the cache.
Platform engineers want to rely solely on GitLab as a universal package manager so that they can reduce costs and drive operational efficiencies. 

## What types of customers are interested in this?

Large, enterprise organizations that need to consolidate on GitLab and move away from Artifactory or Sonatype. To date, it's been difficult for customers like this to migrate away from Artifactory due to a key missing feature.

Virtual registries, which allow you to publish, proxy, and cache multiple package repositories behind a single, logical URL. Without supporting this, no large organization will be able to migrate from Artifactory to GitLab.

The Maven dependency proxy is the MVC of a set of features that will help enterprise organizations sunset Artifactory, reduce costs, and improve the developer user experience.

## Roadmap

1. [Finish the Maven dependency proxy](https://gitlab.com/groups/gitlab-org/-/epics/3610)
1. [npm dependency proxy](https://gitlab.com/groups/gitlab-org/-/epics/3608)
1. [Make the dependency proxy for containers work generically with any container registry](https://gitlab.com/groups/gitlab-org/-/epics/6061)
1. Decide to focus on expanding the above by adding support for multiple upstream repositories (Ultimate) or move this after NuGet
1. [PyPI dependency proxy](https://gitlab.com/groups/gitlab-org/-/epics/3612)
1. [NuGet dependency proxy](https://gitlab.com/groups/gitlab-org/-/epics/3611) 

## Success Criteria

To start, we will be measuring adoption by tier with the following metrics:

1. Number of packages pulled through the dependency proxy
1. The hit ratio (packages pulled from the cache vs. upstream repository)
1. Number of users that pulled a package through the dependency proxy

## Demo

### Prerequisites

1. As of the time of writing this, the feature is behind a feature flag. 
1. The settings for your project must be updated using [GraphQL](https://gitlab.com/-/graphql-explorer).

### Notes

1. The upstream repository for today's demo has been set to Maven central. (Artifactory, Nexus, GitHub, etc. are all supported)

### User flow

1. Open this project's package registry
1. Delete the existing packages for the sake of the demo
1. Update the versions of dependencies in the `pom.xml`
1. Run a pipeline that will install packages using the dependency proxy
1. Notice in the pipeline that the packages have been installed using the dependency proxy
1. Navigate to this project's package registry
1. Notice the new packages and their details


## Other considerations

- This feature can help customers migrate from their existing vendor over time instead of all at once. For example, a Platform engineer could configure the dependency proxy to pull from Artifactory. Over time, more and more packages will have been migrated and be sourced from the GitLab package registry. 
- This feature pairs well with
  - [Maven importer tool](https://gitlab.com/gitlab-org/ci-cd/package-stage/pkgs_importer#maven)
  - [CI events for the package registry](https://gitlab.com/groups/gitlab-org/-/epics/9677) (future)
  - [Dependency firewall](https://gitlab.com/groups/gitlab-org/-/epics/5133) (future)


